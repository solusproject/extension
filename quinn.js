const checkPage = (badWords) => {
    scrollReady = false

    const texts = document.querySelectorAll('p, h1, h2, h3, h4, h5, h6, span, b, q, s, sub, title, li, article, title')
    const images = document.body.querySelectorAll('img[alt], image[alt], video[alt], audio[alt]')
    const links = document.body.querySelectorAll('a[href], button[href], link[href]')
    const metadata = document.querySelectorAll('meta')

    for (badWord of badWords) {
        if (badWord == "" || badWord == "\n") break;
        texts.forEach(node => {
            if (node.textContent.toLowerCase().includes(badWord))
                node.remove()
        })
        images.forEach(node => {
            if (node.alt.toLowerCase().includes(badWord) || node.src.toLowerCase().includes(badWord))
                node.remove()
            else {
                /* @TODO: Add to API the nudity detection */
            }
        })
        links.forEach(node => {
            if (node.href.toLowerCase().includes(badWord) || node.textContent.toLowerCase().includes(badWord))
                node.remove()
        })
        metadata.forEach(node => {
            if (node.content.toLowerCase().includes(badWord)) {
                document.body.innerHTML = "This page has been blocked due to it's sexual nature"
            }
        })
        if (window.location.href.includes(badWord)) {
            document.body.innerHTML = "This page has been blocked due to it's sexual nature"
        }
    }
}

fetch("https://gitlab.com/api/v4/projects/35050367/repository/files/dirty.txt/raw?ref=main").then(response => response.text()).then(data => {
    const badWords = data.split("\n")
    document.addEventListener('load', () => checkPage(badWords))
    if (scrollReady == true) document.addEventListener('scroll', () => checkPage(badWords))
    setInterval(checkPage, 1000, badWords)
    setInterval(scrollReady = true, 100)
});

scrollReady = true;