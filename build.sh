source config.sh

rm -r firefox chromium
mkdir firefox chromium opera build

# Chrome Build
cp chromium.json build/manifest.json

if [ "$DEVELOPMENT" = true ]; then
cp quinn.js build/quinn.out.js
cp background.js build/background.js
else
google-closure-compiler --js quinn.js --js_output_file build/quinn.out.js
google-closure-compiler --js background.js --js_output_file build/background.js
fi

cd build
crx pack -o quinn.crx
mv quinn.crx ../chromium
cd ../

# Opera Build
cp opera.json build/manifest.json

if [ "$DEVELOPMENT" = true ]; then
cp quinn.js build/quinn.out.js
cp background.js build/background.js
else
google-closure-compiler --js quinn.js --js_output_file build/quinn.out.js
google-closure-compiler --js background.js --js_output_file build/background.js
fi

cd build
zip quinn.zip quinn.js background.js manifest.json
mv quinn.zip ../opera
cd ../

# Firefox Build
cp firefox.json build/manifest.json

if [ "$DEVELOPMENT" = true ]; then
cp quinn.js build/quinn.out.js
cp background.js build/background.js
else
google-closure-compiler --js quinn.js --js_output_file build/quinn.out.js
google-closure-compiler --js background.js --js_output_file build/background.js
fi

cd build
web-ext build
if [ "$DEVELOPMENT" = false ]; then
web-ext sign --api-key $ISSUER --api-secret $SECRET
fi
mv web-ext-artifacts ../firefox
cd ../

# Cleanup
rm -r build