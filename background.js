/* Add context menus */
chrome.contextMenus.create({
    id: "report-selection",
    title: "Report to Solus",
    contexts: ["selection"]
});

chrome.contextMenus.onClicked.addListener((info) => {
    if (info.menuItemId == "report-selection") {
        fetch('https://rzoeekuqsybwgflgrgld.supabase.co/rest/v1/reported_messages', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'apikey': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTYyNDA2MjY2NywiZXhwIjoxOTM5NjM4NjY3fQ.RkE66-AsOZlrje6l2n0nRVNLu9JpDXOoU72BuMQO-88',
                'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTYyNDA2MjY2NywiZXhwIjoxOTM5NjM4NjY3fQ.RkE66-AsOZlrje6l2n0nRVNLu9JpDXOoU72BuMQO-88'
            },
            body: JSON.stringify({ url: info.pageUrl, message: info.selectionText }),
        });
    }
});